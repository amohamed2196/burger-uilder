import React from 'react'
import classes from './Burger.css'
import BurgerIngredient from './BurgerIngredient/BurgerIngredient'

const burger = (props) => {
    let TransformedIngredients = 
        Object.keys(props.ingredients).map( igkey => {
            return [...Array(props.ingredients[igkey])].map( (_,i) => 
            {  return <BurgerIngredient key = {igkey + i} type = {igkey} /> });
        })
        .reduce((prev,cur) => { return prev.concat(cur) },[]);
        
    if (TransformedIngredients.length === 0 )
    {
        TransformedIngredients = <p className={classes.p}> Please Add Ingredients </p>;
    }
    return(
       <div className={classes.plate}>
            <div className={classes.Burger} >
            <BurgerIngredient type='bread-top'/>
            {TransformedIngredients}
            <BurgerIngredient type='bread-bottom'/>
        </div>
       </div>
    );
};


export default burger