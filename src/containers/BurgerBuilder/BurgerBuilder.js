import React, {Component} from 'react';
import Aux from '../../hoc/Aux';
import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/BuildControls/BuildControls'

const Ingredients_Prices = {
        meat: 1.2,
        cheese: 0.5,
        salad: 0.2,
        bacon: 0.7
}


class BurgerBuilder extends Component {
    state = {
        Ingredients : {
            meat: 1,
            cheese: 0,
            salad: 0,
            bacon: 0
        },
    totalPrice : 4,
    purchasable  :false  
    }

    updatepurcheState  (ingredient) {
        const sum = Object.keys(ingredient)
        .map(igkey => {
            return ingredient[igkey]
        })
        .reduce((sum,el) => {
            return sum + el; 
        },0 );
        this.setState({purchasable: sum >0})
    }
    addIngredientHandler = (type) => {
        const oldCount = this.state.Ingredients[type];
        const updatedCount = oldCount + 1;
        const updatedIngredients = {
            ...this.state.Ingredients
        }
        updatedIngredients[type]= updatedCount;
        const priceAddition = Ingredients_Prices[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice + priceAddition;
        this.setState({totalPrice : newPrice , Ingredients : updatedIngredients});
        this.updatepurcheState(updatedIngredients);  
    }

   removeIngredientHandler = (type) => {

       const oldCount = this.state.Ingredients[type];
       if (oldCount <=  0)
       {
        return;
       }
        else
        {
            const updatedCount = oldCount - 1;
            const updatedIngredients = {
                ...this.state.Ingredients
            }
            updatedIngredients[type] = updatedCount
            const priceSubtract = Ingredients_Prices[type];
            const oldPrice = this.state.totalPrice;
            const newPrice = oldPrice - priceSubtract;
            this.setState({totalPrice : newPrice , Ingredients : updatedIngredients});
            this.updatepurcheState(updatedIngredients);
        }
       
   } 
    render(){
        const disabledInfoLess = {
            ...this.state.Ingredients
        }
        for (let key in disabledInfoLess)
        {
            if (disabledInfoLess[key] <= 0)
                {
                    disabledInfoLess[key] = true
                }  
                else
                {
                    disabledInfoLess[key] = false 
                }
        }
        const disabledInfomMore = {
            ...this.state.Ingredients
        }
        for (let keyMore in disabledInfomMore)
        {
            disabledInfomMore[keyMore] = disabledInfomMore[keyMore] === 2  
        }

        return(
            <Aux>
                <Burger ingredients={this.state.Ingredients}/>
                <BuildControls
                    ingredientAdded = {this.addIngredientHandler}
                    ingredientSub = {this.removeIngredientHandler}
                    disableLess = {disabledInfoLess}
                    disableMore = {disabledInfomMore}
                    price = {this.state.totalPrice}
                    Purchasable = {this.state.purchasable}
                />
            </Aux>
        )
    }
}

export default BurgerBuilder